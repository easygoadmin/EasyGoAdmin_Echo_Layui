# 基础镜像
FROM golang:1.20
# 镜像作者
MAINTAINER 管理员
# 设置 python 环境变量
ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

# 设置时区
ENV TZ Asia/Shanghai

# 开启模块支持
RUN go env -w GO111MODULE=on
# 设置国内代理
RUN go env -w GOPROXY=https://goproxy.cn,direct
# go env -w GOPROXY=https://mirrors.aliyun.com/goproxy/,direct

# 设置工作区路径
ENV WORKDIR /data/app
# 创建工作目录
RUN mkdir -p $WORKDIR
# 设置工作目录
WORKDIR $WORKDIR
# 添加应用文件
ADD . $WORKDIR
# 安装依赖
CMD go mod tidy
# 构建文件
RUN go build main.go
# 暴露端口
EXPOSE 9057
# 运行文件
ENTRYPOINT ["./main"]
