package main

import (
	"easygoadmin/boot"
	_ "easygoadmin/boot"
	"easygoadmin/conf"
	_ "easygoadmin/conf"
	"easygoadmin/router"
	"github.com/labstack/echo/v4"
)

func main() {
	// 实例化echo对象
	e := echo.New()
	// 开启SESSIOn
	boot.StartSession(e)
	// 路由注册
	router.RegisterRouter(e)
	// 启动应用
	e.Logger.Fatal(e.Start(conf.CONFIG.EGAdmin.Addr + ":" + conf.CONFIG.EGAdmin.Port))
}
