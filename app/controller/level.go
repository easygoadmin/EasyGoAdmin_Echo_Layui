// +----------------------------------------------------------------------
// | EasyGoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2025 深圳EasyGoAdmin研发中心
// +----------------------------------------------------------------------
// | Licensed Apache-2.0 EasyGoAdmin并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.easygoadmin.vip
// +----------------------------------------------------------------------
// | Author: @半城风雨 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package controller

import (
	"easygoadmin/app/dto"
	"easygoadmin/app/model"
	"easygoadmin/app/service"
	"easygoadmin/utils"
	"easygoadmin/utils/common"
	"easygoadmin/utils/gconv"
	"fmt"
	"github.com/gookit/validate"
	"github.com/labstack/echo/v4"
	"net/http"
)

var Level = new(LevelController)

type LevelController struct{}

func (c *LevelController) Index(ctx echo.Context) error {
	// 渲染模板
	return ctx.Render(http.StatusOK, "level_index.html", nil)
}

func (c *LevelController) List(ctx echo.Context) error {
	// 参数
	var req dto.LevelPageReq
	if err := ctx.Bind(&req); err != nil {
		// 返回错误信息
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 调用获取列表方法
	lists, count, err := service.Level.GetList(req)
	if err != nil {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 返回结果集
	return ctx.JSON(http.StatusOK, common.JsonResult{
		Code:  0,
		Data:  lists,
		Msg:   "操作成功",
		Count: count,
	})
}

func (c *LevelController) Edit(ctx echo.Context) error {
	// 查询记录
	id := gconv.Int(ctx.Param("id"))
	info := new(model.Level)
	if id > 0 {
		info = &model.Level{Id: gconv.Int(id)}
		has, err := info.Get()
		if !has || err != nil {
			return ctx.JSON(http.StatusOK, common.JsonResult{
				Code: -1,
				Msg:  err.Error(),
			})
		}
	}
	// 渲染模板
	return ctx.Render(http.StatusOK, "level_edit.html", map[string]interface{}{
		"info": info,
	})
}

func (c *LevelController) Add(ctx echo.Context) error {
	// 添加对象
	var req dto.LevelAddReq
	// 参数绑定
	if err := ctx.Bind(&req); err != nil {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 参数校验
	v := validate.Struct(&req)
	if !v.Validate() {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  v.Errors.One(),
		})
	}
	// 调用添加方法
	rows, err := service.Level.Add(req, utils.Uid(ctx))
	if err != nil || rows == 0 {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 添加成功
	return ctx.JSON(http.StatusOK, common.JsonResult{
		Code: 0,
		Msg:  "添加成功",
	})
}

func (c *LevelController) Update(ctx echo.Context) error {
	// 更新对象
	var req dto.LevelUpdateReq
	// 参数绑定
	if err := ctx.Bind(&req); err != nil {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 参数校验
	v := validate.Struct(&req)
	if !v.Validate() {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  v.Errors.One(),
		})
	}
	// 调用更新方法
	rows, err := service.Level.Update(req, utils.Uid(ctx))
	if err != nil || rows == 0 {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 更新成功
	return ctx.JSON(http.StatusOK, common.JsonResult{
		Code: 0,
		Msg:  "更新成功",
	})
}

func (c *LevelController) Delete(ctx echo.Context) error {
	// 记录ID
	ids := ctx.Param("id")
	if ids == "" {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  "记录ID不能为空",
		})
	}
	// 调用删除方法
	rows, err := service.Level.Delete(ids)
	if err != nil || rows == 0 {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 删除成功
	return ctx.JSON(http.StatusOK, common.JsonResult{
		Code: 0,
		Msg:  "删除成功",
	})
}

func (c *LevelController) Status(ctx echo.Context) error {
	// 设置对象
	var req dto.LevelStatusReq
	// 请求验证
	if err := ctx.Bind(&req); err != nil {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 参数校验
	v := validate.Struct(&req)
	if !v.Validate() {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  v.Errors.One(),
		})
	}
	// 调用设置状态方法
	rows, err := service.Level.Status(req, utils.Uid(ctx))
	if err != nil || rows == 0 {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 设置成功
	return ctx.JSON(http.StatusOK, common.JsonResult{
		Code: 0,
		Msg:  "设置成功",
	})
}

func (c *LevelController) ImportExcel(ctx echo.Context) error {
	// 调用上传方法
	result, err := service.Upload.UploadImage(ctx)
	if err != nil {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 调用文件上传导入方法
	count, err := service.Level.ImportExcel(result.FileUrl, utils.Uid(ctx))
	if err != nil {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 返回结果
	return ctx.JSON(http.StatusOK, common.JsonResult{
		Code: 0,
		Msg:  fmt.Sprintf("本次共导入【%d】条数据", count),
	})
}

func (c *LevelController) ExportExcel(ctx echo.Context) error {
	// 查询对象
	var req dto.LevelPageReq
	// 参数绑定
	if err := ctx.Bind(&req); err != nil {
		// 返回错误信息
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 调用获取列表方法
	fileURL, err := service.Level.GetExcelList(req)
	if err != nil {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  "导出Excel失败",
		})
	}
	// 返回结果集
	return ctx.JSON(http.StatusOK, common.JsonResult{
		Code: 0,
		Msg:  "导出成功",
		Data: fileURL,
	})
}
