// +----------------------------------------------------------------------
// | EasyGoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2025 深圳EasyGoAdmin研发中心
// +----------------------------------------------------------------------
// | Licensed Apache-2.0 EasyGoAdmin并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.easygoadmin.vip
// +----------------------------------------------------------------------
// | Author: @半城风雨 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

/**
 * 文章管理-控制器
 * @author 半城风雨
 * @since 2023-09-02
 * @File : article
 */
package controller

import (
	"easygoadmin/app/dto"
	"easygoadmin/app/model"
	"easygoadmin/app/service"
	"easygoadmin/app/vo"
	"easygoadmin/conf"
	"easygoadmin/utils"
	"easygoadmin/utils/common"
	"easygoadmin/utils/gconv"
	"easygoadmin/utils/gstr"
	"github.com/labstack/echo/v4"
	"net/http"
	"strings"
)

var Article = new(articleCtl)

type articleCtl struct{}

func (c *articleCtl) Index(ctx echo.Context) error {
	// 模板渲染
	return ctx.Render(http.StatusOK, "article_index.html", nil)
}

func (c *articleCtl) List(ctx echo.Context) error {
	// 参数绑定
	var req dto.ArticlePageReq
	if err := ctx.Bind(&req); err != nil {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}

	// 调用获取列表方法
	list, count, err := service.Article.GetList(req)
	if err != nil {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}

	// 返回结果
	return ctx.JSON(http.StatusOK, common.JsonResult{
		Code:  0,
		Data:  list,
		Msg:   "操作成功",
		Count: count,
	})
}

func (c *articleCtl) Edit(ctx echo.Context) error {
	// 记录ID
	id := gconv.Int(ctx.Param("id"))
	// 实例化文章VO对象
	article := vo.ArticleInfoVo{}
	if id > 0 {
		// 编辑
		// 实例化对象
		info := new(model.Article)
		info = &model.Article{Id: gconv.Int(id)}
		has, err := info.Get()
		if !has || err != nil {
			return ctx.JSON(http.StatusOK, common.JsonResult{
				Code: -1,
				Msg:  err.Error(),
			})
		}

		// 文章VO对象
		article.Article = *info

		// 文章封面
		if info.Cover != "" {
			article.Cover = utils.GetImageUrl(info.Cover)
		}

		// 文章图集
		imgList := make([]string, 0)
		if info.Imgs != "" {
			list := gstr.Split(info.Imgs, ",")
			for _, v := range list {
				// 图片地址
				item := utils.GetImageUrl(v)
				imgList = append(imgList, item)
			}
		}
		article.ImgList = imgList

		// 富文本图片替换处理
		if info.Content != "" {
			article.Content = strings.ReplaceAll(info.Content, "[IMG_URL]", conf.CONFIG.EGAdmin.Image)
		}
	} else {
		article.Article = *&model.Article{
			ItemId: 0,
			CateId: 0,
		}
		article.ImgList = make([]string, 0)
	}
	// 渲染模板
	return ctx.Render(http.StatusOK, "article_edit.html", map[string]interface{}{
		"info": article,
	})
}

func (c *articleCtl) Add(ctx echo.Context) error {
	// 参数绑定
	var req dto.ArticleAddReq
	if err := ctx.Bind(&req); err != nil {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}

	// 调用添加方法
	rows, err := service.Article.Add(req, utils.Uid(ctx))
	if err != nil || rows == 0 {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}

	// 返回结果
	return ctx.JSON(http.StatusOK, common.JsonResult{
		Code: 0,
		Msg:  "添加成功",
	})
}

func (c *articleCtl) Update(ctx echo.Context) error {
	// 参数绑定
	var req dto.ArticleUpdateReq
	if err := ctx.Bind(&req); err != nil {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}

	// 调用更新方法
	rows, err := service.Article.Update(req, utils.Uid(ctx))
	if err != nil || rows == 0 {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}

	// 返回结果
	return ctx.JSON(http.StatusOK, common.JsonResult{
		Code: 0,
		Msg:  "更新成功",
	})
}

func (c *articleCtl) Delete(ctx echo.Context) error {
	// 记录ID
	ids := ctx.Param("id")
	if ids == "" {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  "记录ID不能为空",
		})
	}

	// 调用删除方法
	rows, err := service.Article.Delete(ids)
	if err != nil || rows == 0 {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}

	// 返回结果
	return ctx.JSON(http.StatusOK, common.JsonResult{
		Code: 0,
		Msg:  "删除成功",
	})
}

func (c *articleCtl) IsTop(ctx echo.Context) error {
	// 参数绑定
	var req dto.ArticleIsTopReq
	if err := ctx.Bind(&req); err != nil {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}

	// 调用设置状态方法
	rows, err := service.Article.IsTop(req, utils.Uid(ctx))
	if err != nil || rows == 0 {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 返回结果
	return ctx.JSON(http.StatusOK, common.JsonResult{
		Code: 0,
		Msg:  "设置成功",
	})
}

func (c *articleCtl) Status(ctx echo.Context) error {
	// 参数绑定
	var req dto.ArticleStatusReq
	if err := ctx.Bind(&req); err != nil {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}

	// 调用设置状态方法
	rows, err := service.Article.Status(req, utils.Uid(ctx))
	if err != nil || rows == 0 {
		return ctx.JSON(http.StatusOK, common.JsonResult{
			Code: -1,
			Msg:  err.Error(),
		})
	}
	// 返回结果
	return ctx.JSON(http.StatusOK, common.JsonResult{
		Code: 0,
		Msg:  "设置成功",
	})
}
