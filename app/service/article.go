// +----------------------------------------------------------------------
// | EasyGoAdmin敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2025 深圳EasyGoAdmin研发中心
// +----------------------------------------------------------------------
// | Licensed Apache-2.0 EasyGoAdmin并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.easygoadmin.vip
// +----------------------------------------------------------------------
// | Author: @半城风雨 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

/**
 * 文章管理-服务类
 * @author 半城风雨
 * @since 2023-09-02
 * @File : article
 */
package service

import (
	"easygoadmin/app/dto"
	"easygoadmin/app/model"
	"easygoadmin/app/vo"
	"easygoadmin/conf"
	"easygoadmin/utils"
	"easygoadmin/utils/gconv"
	"easygoadmin/utils/gstr"
	"errors"
	"strings"
	"time"
)

// 中间件管理服务
var Article = new(articleService)

type articleService struct{}

func (s *articleService) GetList(req dto.ArticlePageReq) ([]vo.ArticleInfoVo, int64, error) {
	// 初始化查询实例
	query := utils.Engine.Where("mark=1")

	// 文章标题
	if req.Title != "" {
		query = query.Where("title like ?", "%"+req.Title+"%")
	}

	// 是否置顶：1已置顶 2未置顶
	if req.IsTop > 0 {
		query = query.Where("is_top = ?", req.IsTop)
	}

	// 状态：1显示 2隐藏
	if req.Status > 0 {
		query = query.Where("status = ?", req.Status)
	}

	// 排序
	query = query.Asc("id")
	// 分页设置
	offset := (req.Page - 1) * req.Limit
	query = query.Limit(req.Limit, offset)
	// 查询列表
	list := make([]model.Article, 0)
	count, err := query.FindAndCount(&list)

	// 数据处理
	var result []vo.ArticleInfoVo
	for _, v := range list {
		item := vo.ArticleInfoVo{}
		item.Article = v

		// 文章封面
		if v.Cover != "" {
			item.Cover = utils.GetImageUrl(v.Cover)
		}

		// 站点名称
		if v.ItemId > 0 {
			info := &model.Item{Id: v.ItemId}
			has, err2 := info.Get()
			if err2 == nil && has {
				item.ItemName = info.Name
			}
		}

		// 栏目名称
		if v.CateId > 0 {
			cateName := ItemCate.GetCateName(v.CateId, ">>")
			item.CateName = cateName
		}

		result = append(result, item)
	}

	// 返回结果
	return result, count, err
}

func (s *articleService) Add(req dto.ArticleAddReq, userId int) (int64, error) {
	if utils.AppDebug() {
		return 0, errors.New("演示环境，暂无权限操作")
	}
	// 实例化对象
	var entity model.Article

	entity.Title = req.Title
	// 文章封面处理
	if req.Cover != "" {
		cover, err := utils.SaveImage(req.Cover, "article")
		if err != nil {
			return 0, err
		}
		entity.Cover = cover
	}
	//文章图集处理
	if req.Imgs != "" {
		// 图片地址处理
		urlArr := gstr.Split(gconv.String(req.Imgs), ",")
		list := make([]string, 0)
		for _, v := range urlArr {
			if strings.Contains(gconv.String(v), "temp") {
				image, _ := utils.SaveImage(v, "article")
				list = append(list, image)
			} else {
				image := gstr.Replace(v, conf.CONFIG.EGAdmin.Image, "")
				list = append(list, image)
			}
		}
		// 数组转字符串，逗号分隔
		imgs_list := strings.Join(list, ",")
		entity.Imgs = imgs_list
	}
	// 富文本处理
	content := utils.SaveImageContent(req.Content, req.Title, "article")
	entity.Tags = req.Tags
	entity.ItemId = gconv.Int(req.ItemId)
	entity.CateId = gconv.Int(req.CateId)
	entity.IsTop = gconv.Int(req.IsTop)
	entity.ViewNum = gconv.Int(req.ViewNum)
	entity.Author = req.Author
	entity.SourceName = req.SourceName
	entity.SourceUrl = req.SourceUrl
	entity.Guide = req.Guide
	entity.Content = content
	entity.Status = gconv.Int(req.Status)
	entity.CreateUser = userId
	entity.CreateTime = time.Now().Unix()
	entity.Mark = 1
	// 插入数据
	return entity.Insert()
}

func (s *articleService) Update(req dto.ArticleUpdateReq, userId int) (int64, error) {
	if utils.AppDebug() {
		return 0, errors.New("演示环境，暂无权限操作")
	}
	// 查询记录
	entity := &model.Article{Id: gconv.Int(req.Id)}
	has, err := entity.Get()
	if err != nil || !has {
		return 0, errors.New("记录不存在")
	}

	entity.Title = req.Title
	// 文章封面处理
	if req.Cover != "" {
		cover, err := utils.SaveImage(req.Cover, "article")
		if err != nil {
			return 0, err
		}
		entity.Cover = cover
	}
	//文章图集处理
	if req.Imgs != "" {
		// 图片地址处理
		urlArr := gstr.Split(gconv.String(req.Imgs), ",")
		list := make([]string, 0)
		for _, v := range urlArr {
			if strings.Contains(gconv.String(v), "temp") {
				image, _ := utils.SaveImage(v, "article")
				list = append(list, image)
			} else {
				image := gstr.Replace(v, conf.CONFIG.EGAdmin.Image, "")
				list = append(list, image)
			}
		}
		// 数组转字符串，逗号分隔
		imgs_list := strings.Join(list, ",")
		entity.Imgs = imgs_list
	}
	// 富文本处理
	content := utils.SaveImageContent(req.Content, req.Title, "article")
	entity.Tags = req.Tags
	entity.ItemId = gconv.Int(req.ItemId)
	entity.CateId = gconv.Int(req.CateId)
	entity.IsTop = gconv.Int(req.IsTop)
	entity.ViewNum = gconv.Int(req.ViewNum)
	entity.Author = req.Author
	entity.SourceName = req.SourceName
	entity.SourceUrl = req.SourceUrl
	entity.Guide = req.Guide
	entity.Content = content
	entity.Status = gconv.Int(req.Status)
	entity.UpdateUser = userId
	entity.UpdateTime = time.Now().Unix()
	// 更新记录
	return entity.Update()
}

// 删除
func (s *articleService) Delete(ids string) (int64, error) {
	if utils.AppDebug() {
		return 0, errors.New("演示环境，暂无权限操作")
	}
	// 记录ID
	idsArr := strings.Split(ids, ",")
	if len(idsArr) == 1 {
		// 单个删除
		entity := &model.Article{Id: gconv.Int(ids)}
		rows, err := entity.Delete()
		if err != nil || rows == 0 {
			return 0, errors.New("删除失败")
		}
		return rows, nil
	} else {
		// 批量删除
		return 0, nil
	}
}

func (s *articleService) IsTop(req dto.ArticleIsTopReq, userId int) (int64, error) {
	if utils.AppDebug() {
		return 0, errors.New("演示环境，暂无权限操作")
	}
	// 查询记录是否存在
	info := &model.Article{Id: gconv.Int(req.Id)}
	has, err := info.Get()
	if err != nil || !has {
		return 0, errors.New("记录不存在")
	}

	// 设置状态
	entity := &model.Article{}
	entity.Id = info.Id
	entity.IsTop = gconv.Int(req.IsTop)
	entity.UpdateUser = userId
	entity.UpdateTime = time.Now().Unix()
	return entity.Update()
}

func (s *articleService) Status(req dto.ArticleStatusReq, userId int) (int64, error) {
	if utils.AppDebug() {
		return 0, errors.New("演示环境，暂无权限操作")
	}
	// 查询记录是否存在
	info := &model.Article{Id: gconv.Int(req.Id)}
	has, err := info.Get()
	if err != nil || !has {
		return 0, errors.New("记录不存在")
	}

	// 设置状态
	entity := &model.Article{}
	entity.Id = info.Id
	entity.Status = gconv.Int(req.Status)
	entity.UpdateUser = userId
	entity.UpdateTime = time.Now().Unix()
	return entity.Update()
}
